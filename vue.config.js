module.exports = {
  lintOnSave: false,
  publicPath: 'http://localhost:8082/',
  devServer: {
    port: 8082,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
  configureWebpack: {
    output: {
      library: `vueapp`,
      libraryTarget: 'umd' // 把微应用打包成 umd 库格式
    },
  },
}
